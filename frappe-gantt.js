(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("Gantt", [], factory);
	else if(typeof exports === 'object')
		exports["Gantt"] = factory();
	else
		root["Gantt"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = Gantt;
	
	__webpack_require__(1);
	
	var _Bar = __webpack_require__(5);
	
	var _Bar2 = _interopRequireDefault(_Bar);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/* global moment, Snap */
	/**
	 * Gantt:
	 * 	element: querySelector string, HTML DOM or SVG DOM element, required
	 * 	tasks: array of tasks, required
	 *   task: { id, name, start, end, custom_class }
	 * 	config: configuration options, optional
	 */
	function Gantt(element) {
		var tasks = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
		var config = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	
	
		var self = {};
		var row_count = tasks.length > 5 ? tasks.length : 5;
	
		function init() {
			set_defaults();
	
			// expose methods
			self.change_view_mode = change_view_mode;
			self.unselect_all = unselect_all;
			self.view_is = view_is;
			self.get_bar = get_bar;
			self.trigger_event = trigger_event;
			self.refresh = refresh;
			self.animate = animate;
	
			// initialize with default view mode
			change_view_mode(self.config.view_mode);
		}
	
		function set_defaults() {
	
			var merge = __webpack_require__(6);
	
			var defaults = {
				header_height: 50,
				column_width: 30,
				step: 24,
				view_modes: ['Quarter Hour', 'Half Hour', 'Hour', 'Quarter Day', 'Half Day', 'Day', 'Week', 'Month'],
				bar: {
					height: 20
				},
				padding: 18,
				view_mode: 'Day',
				date_format: 'YYYY-MM-DD',
				custom_popup_html: null
			};
			self.config = merge(defaults, config);
	
			reset_variables(tasks);
		}
	
		function reset_variables(tasks) {
			if (typeof element === 'string') {
				self.element = document.querySelector(element);
			} else if (element instanceof SVGElement) {
				self.element = element;
			} else if (element instanceof HTMLElement) {
				self.element = element.querySelector('svg');
			} else {
				throw new TypeError('Frappé Gantt only supports usage of a string CSS selector,' + ' HTML DOM element or SVG DOM element for the \'element\' parameter');
			}
	
			self._tasks = tasks;
	
			self._bars = [];
			self.element_groups = {};
		}
	
		function refresh(updated_tasks) {
			reset_variables(updated_tasks);
			change_view_mode(self.config.view_mode);
		}
	
		function change_view_mode(mode) {
			set_scale(mode);
			prepare();
			render();
			// fire viewmode_change event
			trigger_event('view_change', [mode]);
		}
	
		function prepare() {
			if (self._tasks.length > 0) {
				prepare_tasks();
			}
			prepare_dates();
			prepare_canvas();
		}
	
		function prepare_tasks() {
	
			// prepare tasks
			self.tasks = self._tasks.map(function (task, i) {
	
				// momentify
				task._start = moment(task.start, self.config.date_format);
				task._end = moment(task.end, self.config.date_format);
	
				// make task invalid if duration too large
				if (task._end.diff(task._start, 'years') > 10) {
					task.end = null;
				}
	
				// cache index
				task._index = i;
	
				// invalid dates
				if (!task.start && !task.end) {
					task._start = moment().startOf('day');
					task._end = moment().startOf('day').add(2, 'days');
				}
				if (!task.start && task.end) {
					task._start = task._end.clone().add(-2, 'days');
				}
				if (task.start && !task.end) {
					task._end = task._start.clone().add(2, 'days');
				}
	
				// invalid flag
				if (!task.start || !task.end) {
					task.invalid = true;
				}
	
				// uids
				if (!task.id) {
					task.id = generate_id(task);
				}
	
				return task;
			});
		}
	
		function prepare_dates() {
	
			self.gantt_start = moment();
			self.gantt_end = moment();
			// TODO what's the difference between self._tasks and self.tasks!!!
			for (var i = 0; i < self._tasks.length; i++) {
				var task = self._tasks[i];
				// set global start and end date
				if (task._start < self.gantt_start) {
					self.gantt_start = task._start;
				}
				if (task._end > self.gantt_end) {
					self.gantt_end = task._end;
				}
			}
			set_gantt_dates();
			setup_dates();
		}
	
		function prepare_canvas() {
			if (self.canvas) return;
			self.canvas = Snap(self.element).addClass('gantt');
		}
	
		function render() {
			clear();
			setup_groups();
			make_grid();
			make_dates();
			if (self._tasks.length > 0) {
				make_bars();
			}
			set_width();
			set_scroll_position();
		}
	
		function clear() {
			self.canvas.clear();
			self._bars = [];
		}
	
		function set_gantt_dates() {
	
			if (view_is(['Hour', 'Half Hour', 'Quarter Hour'])) {
				self.gantt_start = self.gantt_start.clone().subtract(1, 'day');
				self.gantt_end = self.gantt_end.clone().add(1, 'day');
			} else if (view_is(['Quarter Day', 'Half Day'])) {
				self.gantt_start = self.gantt_start.clone().subtract(7, 'day');
				self.gantt_end = self.gantt_end.clone().add(7, 'day');
			} else if (view_is('Month')) {
				self.gantt_start = self.gantt_start.clone().startOf('year');
				self.gantt_end = self.gantt_end.clone().endOf('month').add(1, 'year');
			} else {
				self.gantt_start = self.gantt_start.clone().startOf('month').subtract(1, 'month');
				self.gantt_end = self.gantt_end.clone().endOf('month').add(1, 'month');
			}
		}
	
		function setup_dates() {
	
			self.dates = [];
			var cur_date = null;
	
			while (cur_date === null || cur_date < self.gantt_end) {
				if (!cur_date) {
					cur_date = self.gantt_start.clone();
				} else {
					cur_date = view_is('Month') ? cur_date.clone().add(1, 'month') : cur_date.clone().add(self.config.step, 'hours');
				}
				self.dates.push(cur_date);
			}
		}
	
		function setup_groups() {
	
			var groups = ['grid', 'date', 'bar'];
			// make group layers
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;
	
			try {
				for (var _iterator = groups[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var group = _step.value;
	
					self.element_groups[group] = self.canvas.group().attr({ 'id': group });
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		}
	
		function set_scale(scale) {
			self.config.view_mode = scale;
	
			if (scale === 'Day') {
				self.config.step = 24;
				self.config.column_width = 38;
			} else if (scale === 'Half Day') {
				self.config.step = 12; // 24 / 2
				self.config.column_width = 38;
			} else if (scale === 'Quarter Day') {
				self.config.step = 6; // 24 / 4
				self.config.column_width = 38;
			} else if (scale === 'Hour') {
				self.config.step = 1; // 24 / 24
				self.config.column_width = 38;
			} else if (scale === 'Half Hour') {
				self.config.step = 0.5; // 24 / 48
				self.config.column_width = 38;
			} else if (scale === 'Quarter Hour') {
				self.config.step = 0.25; // 24 / 96
				self.config.column_width = 38;
			} else if (scale === 'Week') {
				self.config.step = 168; // 24 * 7
				self.config.column_width = 140;
			} else if (scale === 'Month') {
				self.config.step = 720; // 24 * 30
				self.config.column_width = 120;
			}
		}
	
		function set_width() {
			var cur_width = self.canvas.node.getBoundingClientRect().width;
			var actual_width = self.canvas.select('#date').getBBox().width;
			if (cur_width < actual_width) {
				self.canvas.attr('width', actual_width);
			}
		}
	
		function set_scroll_position() {
			var parent_element = self.element.parentElement;
	
			if (!parent_element) return;
	
			var min_date = self._tasks.length > 0 ? get_min_date() : moment();
			var scroll_pos = min_date.diff(self.gantt_start, 'hours') / self.config.step * self.config.column_width - self.config.column_width;
			parent_element.scrollLeft = scroll_pos;
		}
	
		function get_min_date() {
			var task = self.tasks.reduce(function (acc, curr) {
				return curr._start.isSameOrBefore(acc._start) ? curr : acc;
			});
			return task._start;
		}
	
		function make_grid() {
			make_grid_background();
			make_grid_rows();
			make_grid_header();
			make_grid_ticks();
			make_grid_highlights();
		}
	
		function make_grid_background() {
	
			var grid_width = self.dates.length * self.config.column_width,
			    grid_height = self.config.header_height + self.config.bar.height * row_count;
	
			self.canvas.rect(0, 0, grid_width, grid_height).addClass('grid-background').appendTo(self.element_groups.grid);
	
			self.canvas.attr({
				height: grid_height + self.config.padding + 100,
				width: '100%'
			});
		}
	
		function make_grid_header() {
			var header_width = self.dates.length * self.config.column_width,
			    header_height = self.config.header_height + 10;
			self.canvas.rect(0, 0, header_width, header_height).addClass('grid-header').appendTo(self.element_groups.grid);
		}
	
		function make_grid_rows() {
	
			var rows = self.canvas.group().appendTo(self.element_groups.grid),
			    lines = self.canvas.group().appendTo(self.element_groups.grid),
			    row_width = self.dates.length * self.config.column_width,
			    row_height = self.config.bar.height + self.config.padding;
	
			var row_y = self.config.header_height + self.config.padding / 2;
	
			for (var task in self.tasks) {
				// eslint-disable-line
				self.canvas.rect(0, row_y, row_width, row_height).addClass('grid-row').appendTo(rows);
	
				self.canvas.line(0, row_y + row_height, row_width, row_y + row_height).addClass('row-line').appendTo(lines);
	
				row_y += self.config.bar.height + self.config.padding;
			}
		}
	
		function make_grid_ticks() {
			var tick_x = 0,
			    tick_y = self.config.header_height + self.config.padding / 2,
			    tick_height = (self.config.bar.height + self.config.padding) * row_count;
	
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;
	
			try {
				for (var _iterator2 = self.dates[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var date = _step2.value;
	
					var tick_class = 'tick';
					// thick tick for monday
					if (view_is('Day') && date.day() === 1) {
						tick_class += ' thick';
					}
					// thick tick for first week
					if (view_is('Week') && date.date() >= 1 && date.date() < 8) {
						tick_class += ' thick';
					}
					// thick ticks for quarters
					if (view_is('Month') && date.month() % 3 === 0) {
						tick_class += ' thick';
					}
	
					self.canvas.path(Snap.format('M {x} {y} v {height}', {
						x: tick_x,
						y: tick_y,
						height: tick_height
					})).addClass(tick_class).appendTo(self.element_groups.grid);
	
					if (view_is('Month')) {
						tick_x += date.daysInMonth() * self.config.column_width / 30;
					} else {
						tick_x += self.config.column_width;
					}
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}
		}
	
		function make_grid_highlights() {
	
			// highlight today's date
			// if(view_is('Day')) {
			// TODO
			var time_diff = moment().diff(self.gantt_start, 'hours', true);
			var cols_diff = Math.floor(time_diff / self.config.step);
			var x = cols_diff * self.config.column_width;
			// const time_diff = moment().startOf('day').diff(self.gantt_start, 'hours');
			// const x = time_diff / self.config.step * self.config.column_width;
			var y = 0;
			var width = self.config.column_width;
			var height = (self.config.bar.height + self.config.padding) * row_count + self.config.header_height + self.config.padding / 2;
	
			self.canvas.rect(x, y, width, height).addClass('today-highlight').appendTo(self.element_groups.grid);
			//}
		}
	
		function make_dates() {
			var _iteratorNormalCompletion3 = true;
			var _didIteratorError3 = false;
			var _iteratorError3 = undefined;
	
			try {
	
				for (var _iterator3 = get_dates_to_draw()[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var date = _step3.value;
	
					self.canvas.text(date.lower_x, date.lower_y, date.lower_text).addClass('lower-text').appendTo(self.element_groups.date);
	
					if (date.upper_text) {
						var $upper_text = self.canvas.text(date.upper_x, date.upper_y, date.upper_text).addClass('upper-text').appendTo(self.element_groups.date);
	
						// remove out-of-bound dates
						if ($upper_text.getBBox().x2 > self.element_groups.grid.getBBox().width) {
							$upper_text.remove();
						}
					}
				}
			} catch (err) {
				_didIteratorError3 = true;
				_iteratorError3 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError3) {
						throw _iteratorError3;
					}
				}
			}
		}
	
		function get_dates_to_draw() {
			var last_date = null;
			var dates = self.dates.map(function (date, i) {
				var d = get_date_info(date, last_date, i);
				last_date = date;
				return d;
			});
			return dates;
		}
	
		function get_date_info(date, last_date, i) {
			if (!last_date) {
				last_date = date.clone().add(1, 'year');
			}
			var date_text = {
				'Quarter Day_lower': date.format('HH'),
				'Half Day_lower': date.format('HH'),
				'Hour_lower': date.format('HH'),
				'Half Hour_lower': date.clone().minute(function (m) {
					return m >= 30 ? 30 : 0;
				}(date.minute())).format('HH:mm'),
				'Quarter Hour_lower': date.clone().minute(function (m) {
					if (m < 15) return 0;else if (m < 30) return 15;else if (m < 45) return 30;else return 45;
				}(date.minute())).format('HH:mm'),
				'Day_lower': date.date() !== last_date.date() ? date.format('D') : '',
				'Week_lower': date.month() !== last_date.month() ? date.format('D MMM') : date.format('D'),
				'Month_lower': date.format('MMMM'),
				'Quarter Day_upper': date.date() !== last_date.date() ? date.format('D MMM') : '',
				'Half Day_upper': date.date() !== last_date.date() ? date.month() !== last_date.month() ? date.format('D MMM') : date.format('D') : '',
				'Hour_upper': date.date() !== last_date.date() ? date.format('D MMM') : '',
				'Half Hour_upper': date.date() !== last_date.date() ? date.format('D MMM') : '',
				'Quarter Hour_upper': date.date() !== last_date.date() ? date.format('D MMM') : '',
				'Day_upper': date.month() !== last_date.month() ? date.format('MMMM') : '',
				'Week_upper': date.month() !== last_date.month() ? date.format('MMMM') : '',
				'Month_upper': date.year() !== last_date.year() ? date.format('YYYY') : ''
			};
	
			var base_pos = {
				x: i * self.config.column_width,
				lower_y: self.config.header_height,
				upper_y: self.config.header_height - 25
			};
	
			var x_pos = {
				'Quarter Day_lower': self.config.column_width * 4 / 2,
				'Quarter Day_upper': 0,
				'Half Day_lower': self.config.column_width * 2 / 2,
				'Half Day_upper': 0,
				'Hour_lower': self.config.column_width / 2,
				'Hour_upper': 0,
				'Half Hour_lower': self.config.column_width / 2,
				'Half Hour_upper': 0,
				'Quarter Hour_lower': self.config.column_width / 2,
				'Quarter Hour_upper': 0,
				'Day_lower': self.config.column_width / 2,
				'Day_upper': self.config.column_width * 30 / 2,
				'Week_lower': 0,
				'Week_upper': self.config.column_width * 4 / 2,
				'Month_lower': self.config.column_width / 2,
				'Month_upper': self.config.column_width * 12 / 2
			};
	
			return {
				upper_text: date_text[self.config.view_mode + '_upper'],
				lower_text: date_text[self.config.view_mode + '_lower'],
				upper_x: base_pos.x + x_pos[self.config.view_mode + '_upper'],
				upper_y: base_pos.upper_y,
				lower_x: base_pos.x + x_pos[self.config.view_mode + '_lower'],
				lower_y: base_pos.lower_y
			};
		}
	
		function make_bars() {
	
			self._bars = self.tasks.map(function (task) {
				var bar = (0, _Bar2.default)(self, task);
				self.element_groups.bar.add(bar.group);
				return bar;
			});
		}
	
		function unselect_all() {
			self.canvas.selectAll('.bar-wrapper').forEach(function (el) {
				el.removeClass('active');
			});
		}
	
		function view_is(modes) {
			if (typeof modes === 'string') {
				return self.config.view_mode === modes;
			} else if (Array.isArray(modes)) {
				var _iteratorNormalCompletion4 = true;
				var _didIteratorError4 = false;
				var _iteratorError4 = undefined;
	
				try {
					for (var _iterator4 = modes[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
						var mode = _step4.value;
	
						if (self.config.view_mode === mode) return true;
					}
				} catch (err) {
					_didIteratorError4 = true;
					_iteratorError4 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion4 && _iterator4.return) {
							_iterator4.return();
						}
					} finally {
						if (_didIteratorError4) {
							throw _iteratorError4;
						}
					}
				}
	
				return false;
			}
		}
	
		function get_bar(id) {
			return self._bars.find(function (bar) {
				return bar.task.id === id;
			});
		}
	
		function generate_id(task) {
			return task.name + '_' + Math.random().toString(36).slice(2, 12);
		}
	
		function trigger_event(event, args) {
			if (self.config['on_' + event]) {
				self.config['on_' + event].apply(null, args);
			}
		}
	
		function animate() {
			var speed = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1000;
	
			var highlight = self.canvas.select('rect.today-highlight');
			var w = +self.canvas.attr('width');
			var i = self.config.column_width;
	
			var step = function step() {
				var x = +highlight.attr('x');
				highlight.attr({ 'x': (x + i) % w });
			};
	
			return setInterval(step, speed);
		}
	
		init();
	
		return self;
	}
	module.exports = exports['default'];

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(2);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(4)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../node_modules/css-loader/index.js?sourceMap!../node_modules/sass-loader/index.js?sourceMap!./gantt.scss", function() {
				var newContent = require("!!../node_modules/css-loader/index.js?sourceMap!../node_modules/sass-loader/index.js?sourceMap!./gantt.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(3)();
	// imports
	
	
	// module
	exports.push([module.id, ".gantt .grid-background {\n  fill: #ffffff; }\n\n.gantt .grid-header {\n  fill: #ffffff;\n  stroke: #000;\n  stroke-width: 1.4; }\n\n.gantt .grid-row {\n  fill: #ffffff; }\n\n.gantt .grid-row:nth-child(even) {\n  fill: #f5f5f5; }\n\n.gantt .row-line {\n  stroke: #ebeff2; }\n\n.gantt .tick {\n  stroke: #000;\n  stroke-width: 0.2; }\n  .gantt .tick.thick {\n    stroke-width: 0.4; }\n\n.gantt .today-highlight {\n  fill: #fcf8e3;\n  opacity: 0.5; }\n\n.gantt .bar {\n  fill: green;\n  stroke: #fff;\n  stroke-width: 1;\n  transition: stroke-width .3s ease;\n  stroke-dasharray: 150,5; }\n  .gantt .bar:hover {\n    fill: chartreuse; }\n\n.gantt .bar-invalid {\n  fill: transparent;\n  stroke: #fff;\n  stroke-width: 1;\n  stroke-dasharray: 5; }\n  .gantt .bar-invalid ~ .bar-label {\n    fill: #555; }\n\n.gantt .bar-label {\n  fill: #fff;\n  dominant-baseline: central;\n  text-anchor: middle;\n  font-size: 12px;\n  font-weight: lighter; }\n  .gantt .bar-label.big {\n    fill: #555;\n    text-anchor: start; }\n\n.gantt .bar-wrapper {\n  cursor: pointer; }\n\n.gantt .lower-text, .gantt .upper-text {\n  font-size: 12px;\n  text-anchor: middle; }\n\n.gantt .upper-text {\n  fill: #555; }\n\n.gantt .lower-text {\n  fill: #333; }\n\n.gantt #details .details-container {\n  background: #fff;\n  display: inline-block;\n  padding: 12px; }\n  .gantt #details .details-container h5, .gantt #details .details-container p {\n    margin: 0; }\n  .gantt #details .details-container h5 {\n    font-size: 12px;\n    font-weight: bold;\n    margin-bottom: 10px;\n    color: #555; }\n  .gantt #details .details-container p {\n    font-size: 12px;\n    margin-bottom: 6px;\n    color: #666; }\n  .gantt #details .details-container p:last-child {\n    margin-bottom: 0; }\n\n.gantt .hide {\n  display: none; }\n", "", {"version":3,"sources":["/Users/kas/Dropbox/timeline_proto/gantt/src/src/gantt.scss"],"names":[],"mappings":"AAWA;EAGE,cAAa,EACb;;AAJF;EAME,cAAa;EACb,aAhBiB;EAiBjB,kBAAiB,EACjB;;AATF;EAWE,cAAa,EACb;;AAZF;EAcE,cAtBgB,EAuBhB;;AAfF;EAiBE,gBAxB0B,EAyB1B;;AAlBF;EAoBE,aA7BiB;EA8BjB,kBAAiB,EAIjB;EAzBF;IAuBG,kBAAiB,EACjB;;AAxBH;EA2BE,cAjCoB;EAkCpB,aAAY,EACZ;;AA7BF;EAgCE,YA3Ce;EA4Cf,aA3Ce;EA4Cf,gBAAe;EACf,kCAAiC;EACjC,wBAAuB,EAIvB;EAxCF;IAsCG,iBAAgB,EAChB;;AAvCH;EA0CE,kBAAiB;EACjB,aArDe;EAsDf,gBAAe;EACf,oBAAmB,EAKnB;EAlDF;IAgDG,WApDc,EAqDd;;AAjDH;EAoDE,WAAU;EACV,2BAA0B;EAC1B,oBAAmB;EACnB,gBAAe;EACf,qBAAoB,EAMpB;EA9DF;IA2DG,WA/Dc;IAgEd,mBAAkB,EAClB;;AA7DH;EAiEE,gBAAe,EACf;;AAlEF;EAqEE,gBAAe;EACf,oBAAmB,EACnB;;AAvEF;EAyEE,WA7Ee,EA8Ef;;AA1EF;EA4EE,WA/Ee,EAgFf;;AA7EF;EAgFE,iBAAgB;EAChB,sBAAqB;EACrB,cAAa,EAsBb;EAxGF;IAqFG,UAAS,EACT;EAtFH;IAyFG,gBAAe;IACf,kBAAiB;IACjB,oBAAmB;IACnB,YAhGc,EAiGd;EA7FH;IAgGG,gBAAe;IACf,mBAAkB;IAClB,YAvGc,EAwGd;EAnGH;IAsGG,iBAAgB,EAChB;;AAvGH;EA2GE,cAAa,EACb","file":"gantt.scss","sourcesContent":["$bar-color: green;\n$bar-stroke: #fff;\n$border-color: #000;\n$light-bg: #f5f5f5;\n$light-border-color: #ebeff2;\n$light-yellow: #fcf8e3;\n$text-muted: #666;\n$text-light: #555;\n$text-color: #333;\n$blue: #a3a3ff;\n\n.gantt {\n\n\t.grid-background {\n\t\tfill: #ffffff;\n\t}\n\t.grid-header {\n\t\tfill: #ffffff;\n\t\tstroke: $border-color;\n\t\tstroke-width: 1.4;\n\t}\n\t.grid-row {\n\t\tfill: #ffffff;\n\t}\n\t.grid-row:nth-child(even) {\n\t\tfill: $light-bg;\n\t}\n\t.row-line {\n\t\tstroke: $light-border-color;\n\t}\n\t.tick {\n\t\tstroke: $border-color;\n\t\tstroke-width: 0.2;\n\t\t&.thick {\n\t\t\tstroke-width: 0.4;\n\t\t}\n\t}\n\t.today-highlight {\n\t\tfill: $light-yellow;\n\t\topacity: 0.5;\n\t}\n\n\t.bar {\n\t\tfill: $bar-color;\n\t\tstroke: $bar-stroke;\n\t\tstroke-width: 1;\n\t\ttransition: stroke-width .3s ease;\n\t\tstroke-dasharray: 150,5;\n\t\t&:hover {\n\t\t\tfill: chartreuse;\n\t\t}\n\t}\n\t.bar-invalid {\n\t\tfill: transparent;\n\t\tstroke: $bar-stroke;\n\t\tstroke-width: 1;\n\t\tstroke-dasharray: 5;\n\n\t\t&~.bar-label {\n\t\t\tfill: $text-light;\n\t\t}\n\t}\n\t.bar-label {\n\t\tfill: #fff;\n\t\tdominant-baseline: central;\n\t\ttext-anchor: middle;\n\t\tfont-size: 12px;\n\t\tfont-weight: lighter;\n\n\t\t&.big {\n\t\t\tfill: $text-light;\n\t\t\ttext-anchor: start;\n\t\t}\n\t}\n\n\t.bar-wrapper {\n\t\tcursor: pointer;\n\t}\n\n\t.lower-text, .upper-text {\n\t\tfont-size: 12px;\n\t\ttext-anchor: middle;\n\t}\n\t.upper-text {\n\t\tfill: $text-light;\n\t}\n\t.lower-text {\n\t\tfill: $text-color;\n\t}\n\n\t#details .details-container {\n\t\tbackground: #fff;\n\t\tdisplay: inline-block;\n\t\tpadding: 12px;\n\n\t\th5, p {\n\t\t\tmargin: 0;\n\t\t}\n\n\t\th5 {\n\t\t\tfont-size: 12px;\n\t\t\tfont-weight: bold;\n\t\t\tmargin-bottom: 10px;\n\t\t\tcolor: $text-light;\n\t\t}\n\n\t\tp {\n\t\t\tfont-size: 12px;\n\t\t\tmargin-bottom: 6px;\n\t\t\tcolor: $text-muted;\n\t\t}\n\n\t\tp:last-child {\n\t\t\tmargin-bottom: 0;\n\t\t}\n\t}\n\n\t.hide {\n\t\tdisplay: none;\n\t}\n}\n"],"sourceRoot":""}]);
	
	// exports


/***/ },
/* 3 */
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = Bar;
	/* global Snap */
	/*
		Class: Bar
	
		Opts:
			gt: Gantt object
			task: task object
	*/
	
	function Bar(gt, task) {
	
		var self = {};
	
		function init() {
			set_defaults();
			prepare();
			draw();
			bind();
		}
	
		function set_defaults() {
			self.action_completed = false;
			self.task = task;
		}
	
		function prepare() {
			prepare_values();
			prepare_plugins();
		}
	
		function prepare_values() {
			self.invalid = self.task.invalid;
			self.height = gt.config.bar.height;
			self.x = compute_x();
			self.y = compute_y();
			self.duration = self.task._end.diff(self.task._start, 'hours') / gt.config.step;
			self.width = gt.config.column_width * self.duration;
			self.group = gt.canvas.group().addClass('bar-wrapper').addClass(self.task.custom_class || '');
			self.bar_group = gt.canvas.group().addClass('bar-group').appendTo(self.group);
		}
	
		function prepare_plugins() {
			Snap.plugin(function (Snap, Element, Paper, global, Fragment) {
				Element.prototype.getX = function () {
					return +this.attr('x');
				};
				Element.prototype.getY = function () {
					return +this.attr('y');
				};
				Element.prototype.getWidth = function () {
					return +this.attr('width');
				};
				Element.prototype.getHeight = function () {
					return +this.attr('height');
				};
				Element.prototype.getEndX = function () {
					return this.getX() + this.getWidth();
				};
			});
		}
	
		function draw() {
			draw_bar();
			draw_segment_labels();
			draw_label();
		}
	
		function draw_bar() {
			var block_width = gt.config.column_width * self.task.step / gt.config.step;
			self.$bar = gt.canvas.g().addClass('bar-segments');
			self.$bar.appendTo(self.bar_group);
	
			for (var i = 0; i < self.width; i += block_width) {
				gt.canvas.rect(self.x + i, self.y, block_width, self.height).addClass('bar').attr({ 'data-index': i / block_width }).appendTo(self.$bar);
			}
	
			self.$bar.attr({
				x: self.x,
				y: self.y,
				width: self.width
			});
	
			if (self.invalid) {
				self.$bar.addClass('bar-invalid');
			}
		}
	
		function draw_segment_labels() {
			var block_width = gt.config.column_width * self.task.step / gt.config.step;
			if (block_width < 10) return;
			var x_offset = block_width / 2 - 5,
			    y_offset = self.height / 2 + 3;
	
			self.$label = gt.canvas.g().addClass('segment-labels');
			self.$label.appendTo(self.bar_group);
	
			for (var i = 0, j = 1; i < self.width; j++, i += block_width) {
				gt.canvas.text(self.x + i + x_offset, self.y + y_offset, j).addClass('seg-label');
			}
		}
	
		function draw_label() {
			gt.canvas.text(self.x + self.width / 2, self.y + self.height / 2, self.task.name).addClass('bar-label').appendTo(self.bar_group);
			update_label_position();
		}
	
		function bind() {
			if (self.invalid) return;
			setup_click_event();
		}
	
		function setup_click_event() {
			self.group.selectAll('rect').forEach(function (rect) {
				rect.click(function () {
					if (self.action_completed) {
						// just finished a move action, wait for a few seconds
						return;
					}
	
					gt.trigger_event('click', [rect, self.task]);
				});
			});
		}
	
		function compute_x() {
			var x = self.task._start.diff(gt.gantt_start, 'hours') / gt.config.step * gt.config.column_width;
	
			if (gt.view_is('Month')) {
				x = self.task._start.diff(gt.gantt_start, 'days') * gt.config.column_width / 30;
			}
			return x;
		}
	
		function compute_y() {
			return gt.config.header_height + gt.config.padding + self.task._index * (self.height + gt.config.padding);
		}
	
		function update_label_position() {
			var bar = self.$bar,
			    label = self.group.select('.bar-label');
	
			label.addClass('big').attr('x', bar.getX() - label.getBBox().width);
		}
	
		init();
	
		return self;
	}
	module.exports = exports['default'];

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	(function (global, factory) {
		 true ? module.exports = factory() :
		typeof define === 'function' && define.amd ? define(factory) :
		(global.deepmerge = factory());
	}(this, (function () { 'use strict';
	
	var isMergeableObject = function isMergeableObject(value) {
		return isNonNullObject(value)
			&& !isSpecial(value)
	};
	
	function isNonNullObject(value) {
		return !!value && typeof value === 'object'
	}
	
	function isSpecial(value) {
		var stringValue = Object.prototype.toString.call(value);
	
		return stringValue === '[object RegExp]'
			|| stringValue === '[object Date]'
			|| isReactElement(value)
	}
	
	// see https://github.com/facebook/react/blob/b5ac963fb791d1298e7f396236383bc955f916c1/src/isomorphic/classic/element/ReactElement.js#L21-L25
	var canUseSymbol = typeof Symbol === 'function' && Symbol.for;
	var REACT_ELEMENT_TYPE = canUseSymbol ? Symbol.for('react.element') : 0xeac7;
	
	function isReactElement(value) {
		return value.$$typeof === REACT_ELEMENT_TYPE
	}
	
	function emptyTarget(val) {
		return Array.isArray(val) ? [] : {}
	}
	
	function cloneUnlessOtherwiseSpecified(value, optionsArgument) {
		var clone = !optionsArgument || optionsArgument.clone !== false;
	
		return (clone && isMergeableObject(value))
			? deepmerge(emptyTarget(value), value, optionsArgument)
			: value
	}
	
	function defaultArrayMerge(target, source, optionsArgument) {
		return target.concat(source).map(function(element) {
			return cloneUnlessOtherwiseSpecified(element, optionsArgument)
		})
	}
	
	function mergeObject(target, source, optionsArgument) {
		var destination = {};
		if (isMergeableObject(target)) {
			Object.keys(target).forEach(function(key) {
				destination[key] = cloneUnlessOtherwiseSpecified(target[key], optionsArgument);
			});
		}
		Object.keys(source).forEach(function(key) {
			if (!isMergeableObject(source[key]) || !target[key]) {
				destination[key] = cloneUnlessOtherwiseSpecified(source[key], optionsArgument);
			} else {
				destination[key] = deepmerge(target[key], source[key], optionsArgument);
			}
		});
		return destination
	}
	
	function deepmerge(target, source, optionsArgument) {
		var sourceIsArray = Array.isArray(source);
		var targetIsArray = Array.isArray(target);
		var options = optionsArgument || { arrayMerge: defaultArrayMerge };
		var sourceAndTargetTypesMatch = sourceIsArray === targetIsArray;
	
		if (!sourceAndTargetTypesMatch) {
			return cloneUnlessOtherwiseSpecified(source, optionsArgument)
		} else if (sourceIsArray) {
			var arrayMerge = options.arrayMerge || defaultArrayMerge;
			return arrayMerge(target, source, optionsArgument)
		} else {
			return mergeObject(target, source, optionsArgument)
		}
	}
	
	deepmerge.all = function deepmergeAll(array, optionsArgument) {
		if (!Array.isArray(array)) {
			throw new Error('first argument should be an array')
		}
	
		return array.reduce(function(prev, next) {
			return deepmerge(prev, next, optionsArgument)
		}, {})
	};
	
	var deepmerge_1 = deepmerge;
	
	return deepmerge_1;
	
	})));


/***/ }
/******/ ])
});
;
//# sourceMappingURL=frappe-gantt.js.map