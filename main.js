// main.js
//

// Initialize timeline "Gantt" chart as global variable
window.timeline = new Gantt('#timeline', [], {
	view_mode: 'Hour',
	on_click: function(segment) {
		segment.toggleClass('disabled');
	}
});

// Initialize datetime inputs
(function(dates, times) {
	dates[0].value = moment().format('YYYY-MM-DD');
	dates[1].value = moment().clone().add(1, 'day').format('YYYY-MM-DD');

	times[0].value = moment().format('HH:00');
	times[1].value = moment().add(3, 'hours').format('HH:00');
})( document.querySelectorAll('input[type=date]'),
	document.querySelectorAll('input[type=time]'));


// Initialize forms
(function(els) {

	var details = document.getElementById('runs');

	els.add_form.addEventListener('submit', function (ev) {
		ev.preventDefault();

		var start = moment(els.start_date.value + ' ' + els.start_time.value);
		var end = moment(els.end_date.value + ' ' + els.end_time.value);

		if (end <= start) return alert('Not funny');
		var step = (+els.step_inpt.value) / 60;  // step in days
		var name = 'Run ' + (timeline._tasks.length + 1);

		timeline._tasks.push({
			start: start,
			end: end,
			step: step,
			name: name
		});

		timeline.refresh(timeline._tasks);

        var li = document.createElement('LI');
        li.innerHTML = name + '&nbsp;&nbsp;';

        var btn = document.createElement('BUTTON');
        btn.attributes.type = 'button';
        btn.innerHTML = 'Remove';
        btn.addEventListener('click', function() {
        	var tasks = timeline._tasks.filter(t => t.name !== name);
        	timeline.refresh(tasks);
        	li.parentElement.removeChild(li);
		});

        li.append(btn);
        details.appendChild(li);

	});
})({
	step_inpt: document.getElementById('step-input'),
	add_form: document.getElementById('add-form'),
	end_date: document.getElementById('end-date-input'),
	end_time: document.getElementById('end-time-input'),
	start_date: document.getElementById('start-date-input'),
	start_time: document.getElementById('start-time-input')
});

// Initialize timescale controls
(function(timescale) {
	timescale.addEventListener('change', function() {
		timeline.change_view_mode(this.value);
	});
})(document.getElementById('timescale'));

// Initialize animation controls
(function(btn, speed) {
	var play_txt = 'play', pause_txt = 'pause';
	let int = 0;

	btn.addEventListener('click', function() {
        this.innerHTML = (this.innerHTML === play_txt) ? pause_txt : play_txt;
		if (int === 0) {
            int = timeline.animate(1000 / +speed.value);
            speed.attr('disable', 'disable');
		} else {
            clearInterval(int);
            int = 0;
            speed.attr('disable', false);
        }
	});

})(document.getElementById('play'), document.getElementById('speed'));